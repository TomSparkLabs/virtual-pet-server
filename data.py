import os
import json


def load(filename):
    if not os.path.isfile(filename):
        with open(filename, 'w') as file:
            file.write("{}")
            file.close()

    with open(filename, 'r') as file:
        dump = file.read()
        file.close()
        return dict(json.loads(dump))


def save(data, filename):
    with open(filename, 'w') as file:
        dump = json.dumps(data, indent=True)
        file.write(dump)
        file.close()
