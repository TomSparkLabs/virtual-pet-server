import data
import random
import socketserver
from threading import Thread
from time import sleep
from time import time

pets = data.load("pets.json")
critical_error = False  # something like a "pet panic" of sorts; if something critically trips up


def save():
    data.save(pets, "pets.json")


def register(name):
    def create_unless_existing(name):
        if name not in pets:
            pets[name] = {"dead": False, "sick": False, "health": 100.0, "food": 100.0}

    create_unless_existing(name)


def between(minamount, maxamount, num):
    if num > maxamount:
        num = maxamount
        return num
    elif num < minamount:
        num = minamount
        return num
    else:
        return num


def processing():
    global pets
    for name in pets:
        # load pet variables
        dead = pets[name]["dead"]
        sick = pets[name]["sick"]
        health = pets[name]["health"]
        food = pets[name]["food"]

        if not dead:
            # sickness processing
            if random.randrange(1, 10000000) == 1:
                sick = True
            if sick:
                health -= 0.01

            # health processing
            if health < 100 and not sick:
                health += 0.01
                food -= 0.01

            # food processing
            if random.randrange(1, 5) == 1 and not sick and health == 100:
                food -= 0.001

            # rounding
            health = round(health, 3)
            food = round(food, 3)

            # max/min processing
            health = between(0, 100, health)
            food = between(0, 100, food)

            # death processing
            if health <= 0:
                dead = True

        # save variables
        pets[name]["dead"] = dead
        pets[name]["sick"] = sick
        pets[name]["health"] = health
        pets[name]["food"] = food


def processing_loop():
    process_time = round(time())
    while not critical_error:
        if process_time < (time() - 1):
            # yes I have heard of leap seconds, and I know this code will trip over itself a bit, but this isn't gonna
            # send anybody to space (hopefully), so I'm fine with it
            process_time = round(time())
            processing()
            print(pets)
        sleep(0.01)  # to decrease CPU load, we don't need to query the clock all the time


def save_loop():
    while not critical_error:
        sleep(30)
        save()


# ---------------------------------------------------------------------------------------------------------------------
# where the processing portion ends, and the server portion starts
# ---------------------------------------------------------------------------------------------------------------------

class PetServer(socketserver.BaseRequestHandler):
    def handle(self):
        self.received = self.request.recv(2048).strip().decode("ASCII")
        print(self.received)
        command = self.received.split()

        if not len(self.received) > 30:
            if command[0] == "GET":
                self.request.sendall(str(pets[command[1]][command[2]]).encode("ASCII"))
            elif command[0] == "POST":
                if command[1] == "REGISTER":
                    register(command[2])
                elif command[2] in ["health", "food"]:
                    pets[command[1]][command[2]] = float(command[3])
                elif command[2] in ["dead", "sick"]:
                    pets[command[1]][command[2]] = bool(int(command[3]))

def server_loop():
    server = socketserver.TCPServer(("0.0.0.0", 4444), PetServer)
    print("Server starting...")
    server.serve_forever()

if __name__ == "__main__":
    pet_thread = Thread(target=processing_loop)
    pet_thread.start()
    save_thread = Thread(target=save_loop)
    save_thread.start()
    server_thread = Thread(target=server_loop)
    server_thread.start()
    server_thread.join()
